// Include gulp
var gulp = require('gulp');
var fs = require('fs');
var packagejson = JSON.parse(fs.readFileSync('./package.json'));

var requireDir = require('require-dir');

requireDir('./gulp-tasks');

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(['src/**/*', '!src/{acf-json,acf-json/**}'], gulp.series('build-create'));
    console.log('watching.. press ctrl + c to leave.');
});

// Default Task
gulp.task('default', gulp.series('watch', function() {}));

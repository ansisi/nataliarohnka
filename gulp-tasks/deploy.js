/**
 * Created by Superkrypt.
 * (2016)
 */

var gulp = require('gulp');

var rsync = require('gulp-rsync');
var fs = require('fs');
var packagejson = JSON.parse(fs.readFileSync('./package.json'));

var requireDir = require('require-dir');
requireDir('./');

function deployToEnv(dir, env) {
    return gulp.src(dir)
        .pipe(rsync({
            root: dir,
            hostname: env.host,
            username: env.username,
            destination: env.remotePath,
            compress: true,
            update: true,
            emptyDirectories: true,
            clean: true,
            recursive: true,
            options: {'copy-links': true},
        }));
}
function getDeployToEnvFun(dir, env) {
    return function() {return deployToEnv(dir, env);}
}
gulp.task('deployTst', gulp.series('build-create', getDeployToEnvFun('build/' + packagejson.name, packagejson.envs.tst)));


var ftp = require('vinyl-ftp');
var gutil = require('gulp-util');

gulp.task('deployProd', function(done) {
    for (var i = 0; i < packagejson.envs.prod.length; i++) {
        var remotePath = packagejson.envs.prod[i].remotePath;
        var srcPath = 'release/' + packagejson.name + '-' + packagejson.version;
        var conn = ftp.create({
            host: packagejson.envs.prod[i].host,
            user: packagejson.envs.prod[i].username,
            password: packagejson.envs.prod[i].password,
            log: gutil.log
        });
        var globs = [
            srcPath + '/**/*',
        ];
        gulp.src(globs, { base: srcPath, buffer: false })
            .pipe(conn.dest(remotePath + '/' + packagejson.name + '/'))
            .pipe(conn.dest(remotePath + '/' + packagejson.name + '-' + packagejson.version + '/'));
    };
    done();

});
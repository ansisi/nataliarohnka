/**
 * Created by Superkrypt.
 * (2017)
 */

var gulp = require('gulp');

var svgSymbols = require('gulp-svg-symbols');

var fs = require('fs');
var packagejson = JSON.parse(fs.readFileSync('./package.json'));

gulp.task('sprites', function () {
    return gulp.src('src/theme/img/svg/*.svg')
    .pipe(svgSymbols({templates:['default-svg']}))
    .pipe(gulp.dest('src/theme/img'));
});
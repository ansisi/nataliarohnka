/**
 * Created by Superkrypt.
 * (2017)
 */

var gulp = require('gulp');

var autoprefixer = require('gulp-autoprefixer');
var compass = require('gulp-compass');
var del = require('del');
const sourcemaps = require('gulp-sourcemaps');

var fs = require('fs');
var packagejson = JSON.parse(fs.readFileSync('./package.json'));

module.exports = function (destDir){
    console.log('make css: ' + destDir);
    return gulp.src('src/sass/**/*.scss')
        .pipe(compass({
            // sourcemap: true,
            config_file: './config.rb',
            css: destDir + '/css',
            sass: './src/sass',
        }))
        .on('error', function() {
            this.emit('error');
        })
        // .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['last 2 versions','ie >= 9'],
            cascade: false
        }))
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(destDir + '/css/'))
        .on('end', function() {
            console.log('make css end');
        })
};
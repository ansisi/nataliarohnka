/**
 * Created by Superkrypt.
 * (2017)
 */

var gulp = require('gulp');
var del = require('del');
var fs = require('fs');
var packagejson = JSON.parse(fs.readFileSync('./package.json'));
var makeCss = require('./css');
var makeJs = require('./js');

const BUILD_DIR = 'build/' + packagejson.name;

gulp.task('build-create', function (done) {
    gulp.src(['src/theme/**/*', '!src/theme/img/svg{,/**}'])
        .pipe(gulp.dest(BUILD_DIR));

    makeCss(BUILD_DIR);
    makeJs(BUILD_DIR);

    fs.stat('src/acf-json', function (err, stat) {
        if (err == null) {
            gulp.src('src/acf-json')
                .pipe(gulp.symlink(BUILD_DIR));
        }
    });
    done();
});

gulp.task('build-clean', function (done) {
    return del([BUILD_DIR], done);
});

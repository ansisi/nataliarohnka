/**
 * Created by Superkrypt.
 * (2017)
 */

var gulp = require('gulp');

var favicons = require("gulp-favicons");
var gutil = require("gulp-util");

gulp.task('favicons', function () {
    return gulp
    .src('./src/theme/img/favicon.png')
    .pipe(favicons({
        appName: null,
        appDescription: null,
        developerName: null,
        developerURL: null,
        path: './src/theme/img/',
        url: null,
        display: 'standalone',
        orientation: 'portrait',
        start_url: '/?homescreen=1',
        version: 1.0,
        logging: false,
        online: false,
        icons: {
            android: false,          
            appleIcon: false,            
            appleStartup: false,         
            favicons: true,            
            firefox: false,              
            windows: false,          
            yandex: false              
        },
        html: 'index.html',
        pipeHTML: true,
        replace: true
    }))
    .on('error', gutil.log)
    .pipe(gulp.dest('./src/theme/img/'));
});

/**
 * Created by Superkrypt.
 * (2017)
 */

var gulp = require('gulp');
var pump = require('pump');

var uglify = require('gulp-uglify');
var rename = require("gulp-rename");

module.exports = function (destDir) {
    pump([
        gulp.src('src/js/**/*.js'),
        gulp.dest(destDir + '/js'),
        uglify(),
        rename(function (path) {
            path.basename += ".min";
        }),
        gulp.dest(destDir + '/js')
        ]
    );
};
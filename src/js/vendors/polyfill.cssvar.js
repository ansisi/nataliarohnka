'use strict';
var cssVarPoly = {
    init: function init() {
        if (window.CSS && window.CSS.supports && window.CSS.supports('(--foo: red)')) {
            console.log('your browser supports CSS variables, aborting and letting the native support handle things.');
            return;
        } else {
            console.log('no support for you! polyfill all (some of) the things!!');
            document.querySelector('body').classList.add('cssvars-polyfilled');
        }
        cssVarPoly.ratifiedVars = {};
        cssVarPoly.varsByBlock = {};
        cssVarPoly.oldCSS = {};
        cssVarPoly.findCSS();
        cssVarPoly.updateCSS();
    },
    findCSS: function findCSS() {
        var styleBlocks = document.querySelectorAll('style:not(.inserted),link[type="text/css"]');
        var counter = 1;

        [].forEach.call(styleBlocks, function (block) {
            var theCSS = undefined;
            
            if (block.nodeName === 'LINK') {
                cssVarPoly.getLink(block.getAttribute('href'), counter, function (counter, request) {
                    cssVarPoly.findSetters(request.responseText, counter);
                    cssVarPoly.oldCSS[counter] = request.responseText;
                    cssVarPoly.updateCSS();
                });
                theCSS = '';
            }
            cssVarPoly.oldCSS[counter] = theCSS;
            counter++;
        });
    },
    findSetters: function findSetters(theCSS, counter) {
        var withMedia = theCSS.match(/@media.*?\((max|min)-width:.*px\)\s{([^{])*?{(([^{])*?(--.+:.+;)([^{])*?)?}([^}])*?}/g);
        var cssBlocksConsideringMedia = "";
        
        if (typeof withMedia != "undefined") {
            withMedia.forEach(function(cssBlock) {
                var minWidth = cssBlock.match(/min-width:.*?px/);
                var maxWidth = cssBlock.match(/max-width:.*?px/);

                if (minWidth) minWidth = minWidth[0].substr(11).slice(0, -2);
                if (maxWidth) maxWidth = maxWidth[0].substr(11).slice(0, -2);

                var windowWidth = window.innerWidth;
                if ((!minWidth || windowWidth >= minWidth) && (!maxWidth || windowWidth <= maxWidth)) {
                    cssBlocksConsideringMedia += cssBlock;
                }
            });
        }
        cssVarPoly.varsByBlock[counter] = cssBlocksConsideringMedia.match(/(--.+:.+;)/g);
    },
    updateCSS: function updateCSS() {
        cssVarPoly.ratifySetters(cssVarPoly.varsByBlock);
        for (var curCSSID in cssVarPoly.oldCSS) {
            var newCSS = cssVarPoly.replaceGetters(cssVarPoly.oldCSS[curCSSID], cssVarPoly.ratifiedVars);
            var THEME_DIR = "/wp-content/themes/nataliarohnka/";
            if (!newCSS) newCSS = "";
            newCSS = newCSS.replace(new RegExp("../img", 'g'), THEME_DIR + 'img');
            newCSS = newCSS.replace(new RegExp("../fonts", 'g'), THEME_DIR + 'fonts');
            if (document.querySelector('#inserted' + curCSSID)) {
                document.querySelector('#inserted' + curCSSID).innerHTML = newCSS;
            } else {
                var style = document.createElement('style');
                style.type = 'text/css';
                style.innerHTML = newCSS;
                style.classList.add('inserted');
                style.id = 'inserted' + curCSSID;
                document.getElementsByTagName('head')[0].appendChild(style);
            }
        }
    },
    replaceGetters: function replaceGetters(curCSS, varList) {
        for (var theVar in varList) {
            var getterRegex = new RegExp('var\\(\\s*' + theVar + '\\s*\\)', 'g');
            if (!curCSS) return "";
            curCSS = curCSS.replace(getterRegex, varList[theVar]);
            var getterRegex2 = new RegExp('var\\(\\s*.+\\s*,\\s*(.+)\\)', 'g');
            var matches = curCSS.match(getterRegex2);
            if (matches) {
                matches.forEach(function (match) {
                    curCSS = curCSS.replace(match, match.match(/var\(.+,\s*(.+)\)/)[1]);
                });
            }
        }
        return curCSS;
    },
    ratifySetters: function ratifySetters(varList) {
        for (var curBlock in varList) {
            var curVars = varList[curBlock];
            curVars.forEach(function (theVar) {
                var matches = theVar.split(/:\s*/);
                cssVarPoly.ratifiedVars[matches[0]] = matches[1].replace(/;/, '');
            });
        }
    },
    getLink: function getLink(url, counter, success) {
        var request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.overrideMimeType('text/css;');
        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                if (typeof success === 'function') {
                    success(counter, request);
                }
            } else {
                console.warn('an error was returned from:', url);
            }
        };
        request.onerror = function () {
            console.warn('we could not get anything from:', url);
        };
        request.send();
    }
};
cssVarPoly.init();
var addEvent = function(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on" + type] = callback;
    }
};
addEvent(window, "resize", cssVarPoly.init);
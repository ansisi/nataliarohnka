jQuery(function ($) {
    
    svg4everybody(); 

    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top 
                }, 500);  
                $('.main-header').toggleClass('opened');
                return false;
            }
        }
    });

    $('.show-content').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('content-shown');
    });

    $('.main-header').click(function(e) {
        $(this).toggleClass('opened');
    });

});

<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();

get_template_part('partials/section-offert');

get_template_part('partials/section-contact');

get_footer();

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(' - ', true, 'right'); ?><?php bloginfo(); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/png" sizes="228x228" href="<?php echo get_stylesheet_directory_uri(); ?>/img/coast-228x228.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon-16x16.png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300&amp;subset=latin-ext" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
    <header class="main-header">
        <?php get_template_part('partials/menu'); ?>
    </header>

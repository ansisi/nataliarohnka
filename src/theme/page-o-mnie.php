<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();

get_template_part('partials/section-about-me');

get_template_part('partials/section-edu-achivements');

get_template_part('partials/section-contact');

get_footer();

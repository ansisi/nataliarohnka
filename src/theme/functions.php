<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define('THEME_DIR', get_template_directory_uri());
define('IMG_DIR', THEME_DIR.'/img');

include "admin/custom_element_to_menu.php";
include "admin/add_page_name_to_body_class.php";

include "includes/enqueue_styles_scripts.php";
include "includes/remove-emoji.php";
include "includes/theme_setup.php";

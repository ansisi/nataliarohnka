<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();

get_template_part('partials/section-cover');

get_template_part('partials/section-help');

get_template_part('partials/section-offered_aspects');

get_template_part('partials/section-how-therapy-looks-like');

get_template_part('partials/section-contact');

get_footer();

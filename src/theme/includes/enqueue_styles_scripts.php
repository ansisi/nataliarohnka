<?php

function nataliarohnka_enqueue_styles()
{
    wp_enqueue_style('style', THEME_DIR . '/css/style.css', array());
}

function nataliarohnka_enqueue_scripts()
{
    wp_enqueue_script('svg4everybody', THEME_DIR . '/js/vendors/svg4everybody/svg4everybody.min.js', array("jquery"), false, true);
    wp_enqueue_script('cssvar', THEME_DIR . '/js/vendors/polyfill.cssvar.js', array('jquery'), false, true);
    wp_enqueue_script('app', THEME_DIR . '/js/lib/app.js', array('jquery'), THEME_VERSION, true);
}

function nataliarohnka_minify_scripts( $tag, $handle )
{
    if( is_admin() || strpos( $tag, '.min' )  || !strpos( $tag, 'themes' ) ) {
        return $tag;
    }
    
    return str_replace( '.js', '.min.js', $tag ); 
}

add_action('wp_enqueue_scripts', 'nataliarohnka_enqueue_styles');
add_action('wp_enqueue_scripts', 'nataliarohnka_enqueue_scripts');


if ( defined("ENV_PRODUCTION") ) {
    add_filter('script_loader_tag', 'nataliarohnka_minify_scripts', 10, 2 );
}


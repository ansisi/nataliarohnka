<?php

function bonanova_theme_setup()
{
	add_post_type_support( 'page', 'excerpt' );

    add_theme_support( 'html5', array( 'excerpt', 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

    add_filter('show_admin_bar', '__return_false');

    update_option('image_default_link_type', 'none');

    remove_post_type_support( 'page', 'editor');

    add_image_size( 'large', 1920, 1920, false );

    add_image_size( 'medium', 600, 600, false );

    update_option('thumbnail_size_w', 400);

    update_option('thumbnail_size_h', 400);
}


function remove_menus()
{
    remove_menu_page( 'edit.php' ); 

    remove_menu_page( 'edit-comments.php' );

    remove_menu_page('link-manager.php');
}

add_action( 'admin_menu', 'remove_menus' );

add_action('init', 'bonanova_theme_setup');



function bonanova_register_menu()
{
  register_nav_menu('main-menu',__( 'Menu główne' ));
}
add_action( 'init', 'bonanova_register_menu' );
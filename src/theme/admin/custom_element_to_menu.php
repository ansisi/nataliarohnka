<?php

add_filter('wp_nav_menu_items','add_custom_in_menu', 10, 2);

function add_custom_in_menu( $items, $args ) 
{
    if( $args->theme_location == 'main-menu' )
    {
        $items_array = array();
        while ( false !== ( $item_pos = strpos ( $items, '<li', 3 ) ) )
        {
            $items_array[] = substr($items, 0, $item_pos);
            $items = substr($items, $item_pos);
        }
        $items_array[] = $items;

        array_splice($items_array, 2, 0, '<li class="logo menu-item desktop"><h1>Natalia Rohnka</h1></li>');

        array_splice($items_array, 4, 0, '<li class="menu-item"><a href="#kontakt">Kontakt</a></li>');

        $items = implode('', $items_array);
    }
    return $items;
}
<?php

$frontpage_page_query = new WP_Query( array( 'page_id' => get_page_by_title('Strona główna') -> ID) );
global $wp_query;
$wp_query = $frontpage_page_query;

if (have_posts()) : 

	while (have_posts()) : the_post();

		$contact_image = get_field('contact_bg');
		$contact_title = get_field('contact_title');
		$contact_additional_info = get_field('contact_additional_info');

	endwhile;

endif;

wp_reset_query(); ?>

<section id="kontakt" class="rohnka-section contact-section" style="background-image: url(<?= $contact_image['sizes']['large']; ?>)">

	<div class="page-content contact-form-rohnka">

		<h2 class="contact-title medium-title"><?= $contact_title ?></h2>

		<?php echo do_shortcode('[contact-form-7 id="50" title="Natalia Rohnka kontakt"]'); ?>

		<p class="contact-additional-info">
			
			<?= $contact_additional_info ?>

		</p>

	</div>

</section>

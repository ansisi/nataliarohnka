<section class="rohnka-section edu-achive-section">

	<div class="page-content">

		<div class="edu-achive-intro"><?= get_field('edu_achivements_intro') ?></div>

		<?php if ( have_rows('edu_achivements_field') ) : ?>

			<a href="/" class="show-content">

				<h3 class="edu-achive-title small-title"><?= get_field('edu_achivements_title') ?></h3>

			</a>

			<div class="hidden-list edu-achive-list">

			<?php while ( have_rows('edu_achivements_field') ) : the_row(); ?>

					<h4 class="edu-achive-field-title small-title"><?= get_sub_field('edu_achivement_field_title') ?></h4>

					<?php if ( have_rows('edu_achivement_field_list') ) { ?>

						<ul class="edu-achive-field-list">

							<?php while ( have_rows('edu_achivement_field_list') ) { the_row(); ?>

								<li class="single-edu-achive-item">

									<?= get_sub_field('item_in_achivement') ?>

								</li>

							<?php } ?>

						</ul>

					<?php }

			endwhile; ?>

			</div>

		<?php endif; ?>

	</div>

	<div class="page-content">

		<?php $edu_achivements_outro_image = get_field('edu_achivements_outro_image'); ?>

		<div class="edu-achive-outro">

			<div class="edu-achive-outro-image-wrapper">
			
				<img class="edu-achive-outro-image" src="<?= $edu_achivements_outro_image['sizes']['medium']?>" alt="współpraca"/>

			</div>

			<div class="edu-achive-outro-description">
				
				<?= get_field('edu_achivements_outro') ?>
				
			</div>

		</div>

	</div>

</section>

<section class="rohnka-section how-therapy-looks-like-section">

	<div class="page-content">

		<h2 class="how-therapy-looks-like-title medium-title"><?= get_field('how_therapy_looks_like_title') ?></h2>

		<div class="how-therapy-looks-like-description"><?= get_field('how_therapy_looks_like_description') ?></div>

	</div>

</section>

<section class="rohnka-section help-section">

	<div class="page-content">

		<h2 class="offered-help-title medium-title"><?= get_field('offered_help_title') ?></h2>

		<div class="offered-help-description"><?= get_field('offered_help_description') ?></div>

	</div>

</section>

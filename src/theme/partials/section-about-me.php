<?php 

$about_me_image = get_field('about_me_bg'); 

?>

<section class="rohnka-section about-me-section" style="background-image: url(<?= $about_me_image['sizes']['large']; ?>)">

	<div class="page-content about-me-content">

		<h2 class="about-me-title medium-title"><?= get_field('about_me_title') ?></h2>

		<div class="about-me-description"><?= get_field('about_me_description') ?></div>

		<?php if ( have_rows('list_of_trainings') ) : ?>

			<a href="/" class="show-content">

				<h3 class="trainings-title small-title"><?= get_field('trainings_title') ?></h3>

			</a>

			<ul class="list-of-trainings-list hidden-list">

			<?php while ( have_rows('list_of_trainings') ) : the_row(); ?>

				<li class="single-training">

					<?= get_sub_field('name_of_training') ?>

				</li>

			<?php endwhile; ?>

			</ul>

		<?php endif; ?>

	</div>

</section>

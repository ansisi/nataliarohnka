<?php $offert_image = get_field('offert_bg'); ?>

<section class="rohnka-section offert-section">

	<div class="page-content">

		<div class="offert-description">

			<div class="offert-content">

				<h2 class="offert-title medium-title"><?= get_field('offert_title') ?></h2>

				<?= get_field('offert_description') ?>

			</div>

			<div class="offert-bg-image" style="background-image: url(<?= $offert_image['sizes']['large']; ?>)"></div>

		</div>

	</div>

</section>

<?php $oa_image = get_field('section_bg'); ?>

<section class="rohnka-section offered-aspects-section" style="background-image: url(<?= $oa_image['sizes']['large']; ?>)">

	<div class="page-content">

		<h2 class="oa-title medium-title"><?= get_field('offered_aspects_title') ?></h2>

		<?php if ( have_rows('offered_aspects') ) : ?>

			<ul class="offered-aspects-list">

			<?php while ( have_rows('offered_aspects') ) : the_row(); ?>

				<li class="single-aspect">

					<figure>

						<?php $aspect_icon = get_sub_field('aspect_icon');
						$aspect_name = get_sub_field('aspect_name'); ?>

						<img class="aspect-icon" src="<?= $aspect_icon['sizes']['medium']?>" alt="<?= $aspect_name ?>"/>

						<figcaption class="aspect-name"><?= $aspect_name ?></figcaption>

					</figure>

				</li>

			<?php endwhile; ?>

			</ul>

		<?php endif; ?>

	</div>

</section>

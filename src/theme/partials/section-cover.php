<section class="rohnka-section cover-section">

	<div class="page-content cover-apla">

		<?php $cover_image = get_field('cover_photo'); ?>

		<img src="<?= $cover_image['sizes']['medium'] ?>" alt="<?=  $cover_image['title'] ?>" class="cover-image"/>

		<h2 class="cover-title medium-title"><?= get_field('cover_title') ?></h2>

		<div class="page-content arrow-container mobile">

			<img src="<?= IMG_DIR ?>/arrow.png" alt="scroll" class="scroll-arrow"/>

		</div>

		<p class="cover-description"><?= get_field('cover_description') ?></p>

	</div>

	<div class="page-content arrow-container desktop">

		<img src="<?= IMG_DIR ?>/arrow.png" alt="scroll" class="scroll-arrow"/>

	</div>

</section>
